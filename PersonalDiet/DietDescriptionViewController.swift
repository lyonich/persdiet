//
//  DietDescriptionViewController.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 25.07.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import UIKit
import SDWebImage

var dietName        : String  = ""
var normalYOrigin   : CGFloat = 0.0
var offset          : CGFloat = 0.0

class DietDescriptionViewController: UIViewController {
    @IBOutlet var mainView                      : UIView!
    
    @IBOutlet weak var scrollView               : UIScrollView!
    @IBOutlet weak var imageDietDescription     : UIImageView!
    @IBOutlet weak var caloriesAndGramsLabel    : UILabel!
    @IBOutlet weak var dietDescriptintTextLabel : UILabel!
    @IBOutlet weak var parallaxView             : UIView!

    var diet = DietModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        normalYOrigin = parallaxView.bounds.origin.y
    }

    override func viewWillAppear(_ animated: Bool) {
        mainView.frame.size.height = scrollView.frame.size.height
        scrollView.contentOffset.y = 0
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = dietName
        self.navigationController?.navigationBar.backItem?.title = "Диеты"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Диеты", style: .plain, target: nil, action: nil)
        imageDietDescription.sd_setShowActivityIndicatorView(true)
        imageDietDescription.sd_setIndicatorStyle(.gray)
        imageDietDescription.sd_setImage(with: URL(string : diet.descriptionImage) , completed: nil)
        caloriesAndGramsLabel.text      = diet.caloricityOn100 + " ккал / 100 грамм"
        dietDescriptintTextLabel.text   = diet.text + " Длительность: " + diet.duration
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: - UIScrollViewDelegate

extension DietDescriptionViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if  scrollView.contentOffset.y < offset {
            parallaxView.bounds.origin.y -= 1
        } else {
            parallaxView.bounds.origin.y += 1
        }
        if scrollView.contentOffset.y < 0 {
            parallaxView.bounds.origin.y = normalYOrigin
        }
        offset = scrollView.contentOffset.y
    }
}
