//
//  DietTypeViewController.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 24.07.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import UIKit
import StoreKit
import YandexMobileMetrica
import SDWebImage

var dietIndex = 0

class DietTypeViewController: UIViewController {

    @IBOutlet weak var tableView        : UITableView!
    @IBOutlet weak var greyLoadView     : UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var numberOfItemsInSection  = 0
    var rndDietIndex            = 0
    var dietDescriptionViewController : DietDescriptionViewController!

    var dietArray : [DietModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DietManager.shared.getData(success: { (diets) in
            self.dietArray                  = diets
            self.greyLoadView.isHidden      = true
            self.activityIndicator.isHidden = true
            self.tableView.reloadData()
        }) { (error) in
            print("DATA IS NOT LOAD")
        }
        
        let numberOfEnter = 0
        let parameter = ["NumberOfEnter": numberOfEnter]
        
        if UserDefaults.standard.bool(forKey: "isUserSubscribe") {
            
        }else {
            YMMYandexMetrica.reportEvent("количество входов", parameters: parameter) { (error) in
                print("Did fail report event: количество входов" )
            }
        }

        tableView.dataSource    = self
        tableView.delegate      = self
        tableView.estimatedRowHeight = 44;
        tableView.rowHeight = UITableViewAutomaticDimension

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        dietDescriptionViewController = storyboard.instantiateViewController(withIdentifier: "DietDescriptionViewController") as! DietDescriptionViewController
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//MARK: - Actions 
    
}


//MARK: - UITableViewDataSource

extension DietTypeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if dietArray.count != 0 {
                numberOfItemsInSection = 1
            }
        }else {
            numberOfItemsInSection = dietArray.count
        }
        
        return numberOfItemsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "DietTypeTableViewCell", for: indexPath) as! DietTypeTableViewCell

        let model = dietArray[indexPath.row]
        cell.backgroundColor        = UIColor(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        cell.dietTypeLabel.text     = model.title
        
        let url = URL(string: model.typeImage)!
       
        cell.dietTypeImage.sd_setImage(with: url, completed: nil)
        cell.caloricityLabel.text   = model.dayCaloricity + " ккал в день"
        
        cell.accessoryType = .disclosureIndicator
        
        if indexPath.section == 0 {
            
            dietIndex = UserDefaults.standard.integer(forKey: "rndDietIndex")
            let personalModel = dietArray[dietIndex]
            let personalUrl = URL(string: personalModel.typeImage)!
            cell.backgroundColor = UIColor(colorLiteralRed: 101/255, green: 211/255, blue: 69/255, alpha: 0.3)
    
            cell.dietTypeLabel.text     = personalModel.title
            cell.dietTypeImage.sd_setImage(with: personalUrl, completed: nil)
            cell.caloricityLabel.text   = personalModel.dayCaloricity + " ккал в день"
        }
    
        return cell
    }

    func  numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Персональная диета"
        } else {
            return "Другие рекомендуемые диеты"
        }
    }
}

//MARK: - UITableViewDelegate

extension DietTypeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if UserDefaults.standard.bool(forKey: "isUserSubscribe") {
                dietIndex = UserDefaults.standard.integer(forKey: "rndDietIndex")
            }
        } else {
                dietIndex = indexPath.row
        }
        dietName = dietArray[dietIndex].title
        dietDescriptionViewController.diet = dietArray[dietIndex]
        navigationController?.pushViewController(dietDescriptionViewController, animated: true)
    }
}
