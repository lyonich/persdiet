//
//  DietTypeTableViewCell.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 24.07.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import UIKit

class DietTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var dietTypeLabel    : UILabel!
    @IBOutlet weak var dietTypeImage    : UIImageView!
    @IBOutlet weak var caloricityLabel  : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
