//
//  DietPickingViewController.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 24.07.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class DietPickingViewController: UIViewController {

    var seconds = 5
    var timer = Timer()
    var isTimerRunning = false
    
    var dietTypeViewController : DietTypeViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: (#selector(showDietTypeViewController)), userInfo: nil, repeats: false)
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        dietTypeViewController = storyboard.instantiateViewController(withIdentifier: "DietTypeViewController") as! DietTypeViewController
        
        var indicator: NVActivityIndicatorView!
        indicator = NVActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.width / 2 - 25, y: UIScreen.main.bounds.height / 2 + 32, width: 48, height: 48))
        indicator.type  = .ballClipRotate
        indicator.color = UIColor.green
        indicator.alpha = 1
        indicator.startAnimating()
        
        view.addSubview(indicator)
    }
    
    
    @objc func showDietTypeViewController() {
         navigationController?.pushViewController(dietTypeViewController, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
}
