//
//  PickingViewController.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 24.07.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import UIKit

class PickingViewController: UIViewController {
    
    @IBOutlet weak var viewStep1            : UIView!
    
    @IBOutlet weak var femaleButton         : UIButton!
    @IBOutlet weak var femaleLabel          : UILabel!
    @IBOutlet weak var maleButton           : UIButton!
    @IBOutlet weak var maleLabel            : UILabel!
    

    @IBOutlet weak var viewStep2_6          : UIView!
    
    @IBOutlet weak var pickingType          : UILabel!
    @IBOutlet weak var pickingDescription   : UILabel!
    @IBOutlet weak var pickerView           : UIPickerView!
    @IBOutlet weak var pageControl          : UIPageControl!
    @IBOutlet weak var unitLabel            : UILabel!
    
    @IBOutlet weak var confirmView          : UIView!
    
    @IBOutlet weak var weightLabel          : UILabel!
    @IBOutlet weak var desiredWeightLabel   : UILabel!
    @IBOutlet weak var heightLabel          : UILabel!
    @IBOutlet weak var birthYearLabel       : UILabel!
    @IBOutlet weak var deadlineLabel        : UILabel!
    @IBOutlet weak var sexLabel             : UILabel!

    @IBOutlet weak var pickerButton         : UIButton!
    @IBOutlet weak var confirmButton        : UIButton!
    @IBOutlet weak var confirmLabel         : UILabel!
   
    @IBOutlet weak var topSeparator     : UIView!
    @IBOutlet weak var bottomSeparator  : UIView!
    
    let pickingTypeText : [String]          = ["Укажите ваш вес",
                                               "Укажите желаемый вес",
                                               "Укажите ваш рост",
                                               "Укажите год рождения",
                                               "Желаемый срок достижения веса",
                                               "Выберите ваш пол",
                                               "Подтвердите информацию"]
    
    let pickerDescriptionText : [String]    = ["Рекомендуем взвеситься прямо сейчас",
                                               "Любой результат достижим",
                                               "Введите наиболее точную информацию",
                                               "Наши специалисты подберут оптимальную диету для вашего возраста",
                                               "Мы не рекомендуем выбирать слишком короткий срок",
                                               "Пожалуйста, укажите настоящие данные. Они важны для составления диеты",
                                               "Проверьте введеные вами данные. При необходимости изменения нажмите на нужный пункт"]
    let date                    = Date()
    let calendar                = Calendar.current
    var personBirthYear         : [Int] = []
    let personInfo              : [Int] = Array(45...300)
    let monthsToWeight          : [Int] = Array(1...72)
    //количество компонентов в пиккере
    var numberOfComponents      : Int = 0
    //строки в пиккере
    var pickerRowText           : String = ""
    var screenCounter           = 0
    var giftViewController      : GiftViewController!
    var isThisCorrection        = false
    
    var lastWeightIndex         = 5
    var lastDesiredWeightIndex  = 5
    var lastHeightIndex         = 105
    var lastBirthYearIndex      = 40
    var lastDeadlineIndex       = 7
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenHeight = UIScreen.main.bounds.height
        let yearPicker = calendar.dateComponents([.year], from: date)
        let thisYear = yearPicker.year!
        personBirthYear  = Array(1950...thisYear)
        
        pickerView.selectRow(5, inComponent: 0, animated: false)
        pageControl.pageIndicatorTintColor = UIColor(colorLiteralRed: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        pageControl.currentPageIndicatorTintColor = UIColor(colorLiteralRed: 92/255, green: 193/255, blue: 72/255, alpha: 1)
        
        updateTitles()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        giftViewController = storyboard.instantiateViewController(withIdentifier: "GiftViewController") as! GiftViewController
        
        pickerButton.layer.cornerRadius     = UIScreen.main.bounds.size.height/23
        confirmButton.layer.cornerRadius    = UIScreen.main.bounds.size.height/23
        pickingDescription.font = UIFont(name:pickingDescription.font.fontName , size: screenHeight/48)
        
        pickerView.dataSource  = self
        pickerView.delegate    = self
        
        viewStep1.isHidden      = true
        viewStep2_6.isHidden    = false
        confirmView.isHidden    = true
        confirmButton.isHidden  = true
        confirmLabel.isHidden   = true
        
        maleButton.alpha = 0.5
        maleLabel.alpha  = 0.5
        sexLabel.text = "Женский"
        
        femaleButton.alpha = 1
        femaleLabel.alpha  = 1
        
        topSeparator.frame.origin.y     = UIScreen.main.bounds.size.height/11.5
        bottomSeparator.frame.origin.y  = UIScreen.main.bounds.size.height/20.1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//MARK: - Actions
    
    @IBAction func pickerButtonPressed(_ sender: Any) {
        //"Укажите ваш вес"
        if pageControl.currentPage == 0 {
            if desiredWeightLabel.text == "" {
                pickerView.selectRow(lastWeightIndex, inComponent: 0, animated: false)
                weightLabel.text = "\(personInfo[lastWeightIndex]) кг "
            }
        }
        
        pageControl.currentPage += 1
        screenCounter += 1
        pickerView.selectRow(0, inComponent: 0, animated: false)
        
        if isThisCorrection == true {
                screenCounter = 6
        }
        
        //"Укажите желаемый вес"
        if pageControl.currentPage == 1 {
            if desiredWeightLabel.text == "" {
                    pickerView.selectRow(lastDesiredWeightIndex, inComponent: 0, animated: false)
                    desiredWeightLabel.text = "\(personInfo[lastDesiredWeightIndex]) кг"
            }
        }
        // "Укажите ваш рост"
        if pageControl.currentPage == 2 {
            if  heightLabel.text == "" {
                    pickerView.selectRow(lastHeightIndex, inComponent: 0, animated: false)
                    heightLabel.text = "\(personInfo[lastHeightIndex]) см"
            }
        }
        // "Укажите год рождения"
        if pageControl.currentPage == 3 {
            if birthYearLabel.text == "" {
                    pickerView.selectRow(lastBirthYearIndex, inComponent: 0, animated: false)
                    birthYearLabel.text = "\(personBirthYear[lastBirthYearIndex])"
            }
        }
        //"Желаемый срок достижения веса"
        if pageControl.currentPage == 4 {
            if  deadlineLabel.text == "" {
                    pickerView.selectRow(lastDeadlineIndex, inComponent: 0, animated: false)
                    deadlineLabel.text = "\(monthsToWeight[lastDeadlineIndex]) недель"
            }
        }
        
        if pageControl.currentPage == 5 {
            viewStep1.isHidden      = false
            viewStep2_6.isHidden    = true
        }
        
        if screenCounter == 6 {
            viewStep1.isHidden      = true
            viewStep2_6.isHidden    = true
            confirmView.isHidden    = false
            
            pageControl.isHidden    = true
            pickerButton.isHidden   = true
            confirmButton.isHidden  = false
        }
        updateTitles()
        
        pickerView.reloadAllComponents()
    }
    
    func updateTitles() {
        if screenCounter >= 6 {
            pickingType.text        = pickingTypeText[screenCounter]
            pickingDescription.text = pickerDescriptionText[screenCounter]
        } else {
            pickingType.text        = pickingTypeText[pageControl.currentPage]
            pickingDescription.text = pickerDescriptionText[pageControl.currentPage]
        }
    }
    
    @IBAction func femaleButtonPressed(_ sender: Any) {
        maleButton.alpha = 0.5
        maleLabel.alpha  = 0.5
        sexLabel.text = "Женский"
        femaleButton.alpha = 1
        femaleLabel.alpha  = 1
    }
    
    @IBAction func maleButtonPressed(_ sender: Any) {
        femaleButton.alpha = 0.5
        femaleLabel.alpha  = 0.5
        sexLabel.text = "Мужской"
        maleButton.alpha = 1
        maleLabel.alpha  = 1
    }

    @IBAction func confirmButtonPressed(_ sender: Any) {
        navigationController?.pushViewController(giftViewController, animated: true)
    }
    
    @IBAction func weightButtonPressed(_ sender: Any) {
        pageControl.currentPage = 0
        screenCounter = 0
        updateTitles()
        showAndHideObjects()
        pickerView.selectRow(lastWeightIndex, inComponent: 0, animated: false)
    }
    
    @IBAction func desiredWeightButtonPressed(_ sender: Any) {
        pageControl.currentPage = 1
        screenCounter = 1
        updateTitles()
        showAndHideObjects()
        pickerView.selectRow(lastDesiredWeightIndex, inComponent: 0, animated: false)
    }
    
    @IBAction func changeHeightButtonPressed(_ sender: Any) {
        pageControl.currentPage = 2
        screenCounter = 2
        updateTitles()
        showAndHideObjects()
        pickerView.selectRow(lastHeightIndex, inComponent: 0, animated: false)
    }

    @IBAction func birthYearButtonPressed(_ sender: Any) {
        pageControl.currentPage = 3
        screenCounter = 3
        updateTitles()
        showAndHideObjects()
        pickerView.selectRow(lastBirthYearIndex, inComponent: 0, animated: false)
    }
    
    @IBAction func deadlineButtonPressed(_ sender: Any) {
        pageControl.currentPage = 4
        screenCounter = 4
        updateTitles()
        showAndHideObjects()
        pickerView.selectRow(lastDeadlineIndex, inComponent: 0, animated: false)
    }
    
    @IBAction func changeSexButtonPressed(_ sender: Any) {
        pageControl.currentPage = 5
        screenCounter = 5
        updateTitles()
        pickerView.reloadAllComponents()
        isThisCorrection = true

        viewStep1.isHidden      = false
        viewStep2_6.isHidden    = true
        confirmView.isHidden    = true
        pageControl.isHidden    = false
        pickerButton.isHidden   = false
        confirmButton.isHidden  = true
        confirmLabel.isHidden   = true
    }
    
    func showAndHideObjects() {
        pickerView.reloadAllComponents()
        isThisCorrection = true
        viewStep2_6.isHidden    = false
        confirmView.isHidden    = true
        pageControl.isHidden    = false
        pickerButton.isHidden   = false
        confirmButton.isHidden  = true
        confirmLabel.isHidden   = true
    }
}

//MARK: - UIPickerViewDataSource

extension PickingViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pageControl.currentPage == 0 {
            numberOfComponents = personInfo.count
        }
        if pageControl.currentPage == 1 {
            numberOfComponents = personInfo.count
        }
        if pageControl.currentPage == 2 {
            numberOfComponents = personInfo.count
        }
        if pageControl.currentPage == 3 {
            numberOfComponents = personBirthYear.count
        }
        if pageControl.currentPage == 4 {
            numberOfComponents = monthsToWeight.count
        }
        
        return numberOfComponents
    }
}

//MARK: - UIPickerViewDelegate

extension PickingViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pageControl.currentPage == 0 {
            weightLabel.text    = "\(personInfo[row]) кг"
            lastWeightIndex     = row
        }
        if pageControl.currentPage == 1 {
            desiredWeightLabel.text = "\(personInfo[row]) кг"
            lastDesiredWeightIndex  = row
        }
        if pageControl.currentPage == 2 {
            heightLabel.text    = "\(personInfo[row]) см"
            lastHeightIndex     = row
        }
        if pageControl.currentPage == 3 {
            birthYearLabel.text = "\(personBirthYear[row])"
            lastBirthYearIndex  = row
        }
        if pageControl.currentPage == 4 {
            deadlineLabel.text  = "\(monthsToWeight[row]) недель"
            lastDeadlineIndex   = row
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let pickerLabel = UILabel()
        if pageControl.currentPage == 0 {
            pickerLabel.text = "\(personInfo[row])"
            unitLabel.text = "кг"
        }
        if pageControl.currentPage == 1 {
            pickerLabel.text = "\(personInfo[row])"
            unitLabel.text = "кг"
        }
        if pageControl.currentPage == 2 {
            pickerLabel.text = "\(personInfo[row])"
            unitLabel.text = "см."
        }
        if pageControl.currentPage == 3 {
            pickerLabel.text = "\(personBirthYear[row])"
            unitLabel.text = ""
        }
        if pageControl.currentPage == 4 {
            pickerLabel.text = "\(monthsToWeight[row])"
            unitLabel.text = "недель"
        }
        pickerLabel.textColor       = UIColor.black
        pickerLabel.font            = UIFont(name: pickerLabel.font.fontName, size: 32)
        pickerLabel.textAlignment   = NSTextAlignment.center

        return pickerLabel
    }
}
