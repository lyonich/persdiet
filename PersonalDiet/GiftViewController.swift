//
//  GiftViewController.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 24.07.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import UIKit
import FacebookCore

class GiftViewController: UIViewController {

    
    
    @IBOutlet weak var termAndPrivacyTextView   : UITextView!
    
    @IBOutlet weak var giftButton           : UIButton!
    
    @IBOutlet weak var webView                  : UIWebView!
    @IBOutlet weak var closeWebViewButton       : UIButton!
    
    @IBOutlet weak var giftTopConstrain     : NSLayoutConstraint!
    @IBOutlet weak var giftBottomConstrain  : NSLayoutConstraint!
    @IBOutlet weak var buttonBotConstrain   : NSLayoutConstraint!
    
    var dietPickingViewController   : DietPickingViewController!
    var dontRushController          : DoNotRushViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenHeight = UIScreen.main.bounds.height
        giftTopConstrain.constant       = screenHeight / 4
        giftBottomConstrain.constant    = screenHeight / 7
        buttonBotConstrain.constant     = screenHeight / 20
        webView.isHidden                = true
        closeWebViewButton.isHidden     = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        dietPickingViewController = storyboard.instantiateViewController(withIdentifier: "DietPickingViewController") as! DietPickingViewController
        dontRushController = storyboard.instantiateViewController(withIdentifier: "DoNotRushViewController") as! DoNotRushViewController

        giftButton.layer.cornerRadius = UIScreen.main.bounds.size.height/23
    
        let text = "Продолжая использование приложения, я соглашаюсь с пользовательским соглашением и политикой конфиденциальности"
        let underlineAttriString = NSMutableAttributedString(string : text)
        let rangeOfTerms   = (text as NSString).range(of: "пользовательским соглашением")
        let rangeOfPrivacy = (text as NSString).range(of: "политикой конфиденциальности")
        
        let termsLinkAttributes     = [
            NSLinkAttributeName : NSURL(string : "http://tnx.net/eula")!, NSForegroundColorAttributeName : UIColor.blue
            ] as [String : Any]
        let privacyLinkAttributes   = [
            NSLinkAttributeName : NSURL(string : "http://tnx.net/pp")!, NSForegroundColorAttributeName : UIColor.blue
            ] as [String : Any]
        
        underlineAttriString.setAttributes(termsLinkAttributes, range: rangeOfTerms)
        underlineAttriString.setAttributes(privacyLinkAttributes, range: rangeOfPrivacy)
        
        self.termAndPrivacyTextView.delegate        = self
        self.termAndPrivacyTextView.attributedText  = underlineAttriString
        self.termAndPrivacyTextView.textAlignment   = .center
        termAndPrivacyTextView.font = UIFont(name: (termAndPrivacyTextView.font?.fontName)!, size: 10)
        termAndPrivacyTextView.textColor = UIColor.gray
        
        StoreManager.shared.delegate            = self
        StoreManager.shared.isLoadGiftScreen    = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//MARK: - Actions
    
    @IBAction func giftButtonPressed(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork() == true {
            if StoreManager.shared.productsFromStore.count > 0 {
                let product = StoreManager.shared.productsFromStore[0]
                StoreManager.shared.buy(product: product)
            } else {
                let alert = UIAlertView(title: "ERROR", message: "Сould not retrieve purchase data", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        } else {
            print("Internet connection FAILED")
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    @IBAction func closeWebViewButtonPressed(_ sender: Any) {
        closeWebViewButton.isHidden = true
        webView.isHidden            = true
    }
}

//MARK: - subscribeDelegate

extension GiftViewController: subscribeDelegate {
    func userDidsubscribe(isUserSubscribe : Bool) {
        if dietPickingViewController == nil {
            dietPickingViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DietPickingViewController") as! DietPickingViewController
        }
         navigationController?.pushViewController(dietPickingViewController, animated: true)
         UserDefaults.standard.set(true, forKey: "isUserSubscribe")
         UserDefaults.standard.set(rndDietIndex, forKey: "rndDietIndex")
        
         AppEventsLogger.log("Subscribe")
    }
    
    func didPressCancel() {
        if dontRushController.isViewLoaded == false {
           navigationController?.pushViewController(dontRushController, animated: true)
        }
        UserDefaults.standard.set(rndDietIndex, forKey: "rndDietIndex")
    }
}

//MARK: - UITextViewDelegate

extension GiftViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        closeWebViewButton.isHidden = false
        webView.isHidden            = false
        
        let request = NSURLRequest(url: URL)
        webView.loadRequest(request as URLRequest)
        
        return false
    }
}
