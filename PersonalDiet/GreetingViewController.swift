//
//  GreetingViewController.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 24.07.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import UIKit

class GreetingViewController: UIViewController {

    @IBOutlet weak var termAndPrivacyTextView   : UITextView!
    @IBOutlet weak var webView                  : UIWebView!
    @IBOutlet weak var buttonTop                : NSLayoutConstraint!
    @IBOutlet weak var imageBot                 : NSLayoutConstraint!
    @IBOutlet weak var slimButton               : UIButton!
    @IBOutlet weak var closeWebViewButton       : UIButton!
    
    let termURL      = NSMutableAttributedString(string: "пользовательским соглашением")
    let privacyURL   = NSMutableAttributedString(string: "политикой конфиденциальности")
    
    var pickingViewController       : PickingViewController!
    var dietTypeViewController      : DietTypeViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let text = "Продолжая использование приложения, я соглашаюсь с пользовательским соглашением и политикой конфиденциальности"
        let underlineAttriString = NSMutableAttributedString(string : text)
        let rangeOfTerms   = (text as NSString).range(of: "пользовательским соглашением")
        let rangeOfPrivacy = (text as NSString).range(of: "политикой конфиденциальности")
        
        let termsLinkAttributes     = [
            NSLinkAttributeName : NSURL(string : "http://tnx.net/eula")!, NSForegroundColorAttributeName : UIColor.blue
        ] as [String : Any]
        let privacyLinkAttributes   = [
            NSLinkAttributeName : NSURL(string : "http://tnx.net/pp")!, NSForegroundColorAttributeName : UIColor.blue
            ] as [String : Any]
        
        underlineAttriString.setAttributes(termsLinkAttributes, range: rangeOfTerms)
        underlineAttriString.setAttributes(privacyLinkAttributes, range: rangeOfPrivacy)

        self.termAndPrivacyTextView.delegate        = self
        self.termAndPrivacyTextView.attributedText  = underlineAttriString
        self.termAndPrivacyTextView.textAlignment   = .center
        termAndPrivacyTextView.font = UIFont(name: (termAndPrivacyTextView.font?.fontName)!, size: 10)
        termAndPrivacyTextView.textColor = UIColor.gray
        webView.isHidden            = true
        closeWebViewButton.isHidden = true
        
        let screenHeight = UIScreen.main.bounds.height
        buttonTop.constant = screenHeight / 10
        imageBot.constant  = screenHeight / 20
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        dietTypeViewController = storyboard.instantiateViewController(withIdentifier: "DietTypeViewController") as! DietTypeViewController
        pickingViewController = storyboard.instantiateViewController(withIdentifier: "PickingViewController") as! PickingViewController
        
       self.navigationController?.isNavigationBarHidden = true
        
       slimButton.layer.cornerRadius = UIScreen.main.bounds.size.height/23
        
        if UserDefaults.standard.bool(forKey: "isUserSubscribe") {
            navigationController?.pushViewController(dietTypeViewController, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//MARK: - Actions
    
    @IBAction func closeWebViewButtonPressed(_ sender: Any) {
        closeWebViewButton.isHidden = true
        webView.isHidden            = true
    }
    
    @IBAction func slimButtonPressed(_ sender: Any) {
         navigationController?.pushViewController(pickingViewController, animated: true)
    }
}

//MARK: - UITextViewDelegate

extension GreetingViewController: UITextViewDelegate {

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        closeWebViewButton.isHidden = false
        webView.isHidden            = false
        
        let request = NSURLRequest(url: URL)
        webView.loadRequest(request as URLRequest)
        
        return false
    }
}
