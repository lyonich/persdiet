//
//  DietData.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 15.08.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class DietModel {
    var title              : String = ""
    var dayCaloricity      : String = ""
    var caloricityOn100    : String = ""
    var duration           : String = ""
    var text               : String = ""
    var typeImage          : String = ""
    var descriptionImage   : String = ""
}

let rndDietIndex = arc4random_uniform(31)

class DietManager: NSObject {

    static let shared = DietManager()
    
    func getData (success: @escaping ([DietModel]) -> Void, failure: @escaping (NSError) -> Void) {
        Alamofire.request("http://94.130.12.179/get.php", method: .get, parameters: nil, encoding: URLEncoding.httpBody).responseJSON { (response) in
            switch response.result {
            case .success(let value) :
                let json = JSON(value)
                var diets = [DietModel]()
                for jsonData in json {
                    diets.append(self.parseUserResponse(json: jsonData.1))
                }
                success(diets)

            case .failure(_) :
                print (response.response?.statusCode)
            }
        }
    }

    func parseUserResponse(json: JSON) -> DietModel {
        let diet = DietModel()
        diet.title              = json["title"].stringValue
        diet.dayCaloricity      = json["day_caloricity"].stringValue
        diet.caloricityOn100    = json["caloricity_100"].stringValue
        diet.duration           = json["duration"].stringValue
        diet.text               = json["text"].stringValue
        diet.typeImage          = json["imgs"][0].stringValue
        diet.descriptionImage   = json["imgs"][1].stringValue
        
        return diet
    }
}
