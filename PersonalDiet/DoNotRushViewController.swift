//
//  DoNotRushViewController.swift
//  PersonalDiet
//
//  Created by Илья Медведев on 26.07.17.
//  Copyright © 2017 Илья Медведев. All rights reserved.
//

import UIKit
import FacebookCore

class DoNotRushViewController: UIViewController {

    @IBOutlet weak var getFreeAccessButton  : UIButton!
    @IBOutlet weak var useYourChanceLabel   : UILabel!
    
    var giftViewController      : GiftViewController!
    var dietTypeViewController  : DietTypeViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenHeight = UIScreen.main.bounds.height
        useYourChanceLabel.font =  UIFont(name:useYourChanceLabel.font.fontName , size: screenHeight/25)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        giftViewController = storyboard.instantiateViewController(withIdentifier: "GiftViewController") as! GiftViewController
        dietTypeViewController = storyboard.instantiateViewController(withIdentifier: "DietTypeViewController") as! DietTypeViewController
        getFreeAccessButton.layer.cornerRadius = UIScreen.main.bounds.size.height/23
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//MARK: - Actions
    
    @IBAction func getFreeAccessButtonPressed(_ sender: Any) {
        let product = StoreManager.shared.productsFromStore[0]
        StoreManager.shared.buy(product: product)
    }
    
    @IBAction func notConvencedButoonPressed(_ sender: Any) {
        let product = StoreManager.shared.productsFromStore[0]
        StoreManager.shared.buy(product: product)
    }

}

//MARK: - subscribeDelegate

extension DoNotRushViewController: subscribeDelegate {
    func userDidsubscribe(isUserSubscribe : Bool) {
        navigationController?.pushViewController(dietTypeViewController, animated: true)
        UserDefaults.standard.set(true, forKey: "isUserSubscribe")
        UserDefaults.standard.set(rndDietIndex, forKey: "rndDietIndex")
        
        AppEventsLogger.log("Subscribe")
    }
    
    func didPressCancel() {
        UserDefaults.standard.set(rndDietIndex, forKey: "rndDietIndex")
    }
}
